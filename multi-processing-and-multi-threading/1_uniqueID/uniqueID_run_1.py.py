from multiprocessing import Pool
import time
from urllib.request import urlopen
from bs4 import BeautifulSoup


def uniqueID(n):
    url = "https://www.uuidgenerator.net/api/version1"
    html = urlopen(url).read()
    soup = BeautifulSoup(html, features="html.parser")
    print(soup)


if __name__ == '__main__':
    num = int(input('enter number: '))
    start_time = time.time()
    pool = Pool(num)
    pool.map(uniqueID, range(num))
    end_time = time.time()
    print('time:', end_time - start_time)
