#### EXERCISE 1:
Fetch unique id from https://www.uuidgenerator.net/api/version1

###### EXAMPLE
* input: n=5
* output:
    * ba42e844-87c5-11eb-8dcd-0242ac130003
    * 284ebfb6-87c6-11eb-8dcd-0242ac130003
    * 2b18f6e4-87c6-11eb-8dcd-0242ac130003
    * 2d084978-87c6-11eb-8dcd-0242ac130003
    * 2f0423dc-87c6-11eb-8dcd-0242ac130003
    
#### UNDERSTANDING:

I need to fetch text of given url and need to print this as 
per user. 
instead of using serializing approach we need to perform this process parallel approach

#### EXECUTION:
* create a function to read and print the unique id from given url
* inside the function  
    * open given Url and read it
    * fetch the text from html and print it
* inside main
    * take number input from user
    * create a pool for user input and map that inside the function 
    * print end time minus start time to check time taken to fetch all id.


