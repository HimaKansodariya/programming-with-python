import pandas as pd
import matplotlib.pyplot as plt

# reading all data of .csv file and store
data = pd.read_csv('job_details.csv')

# saperating integer value of job publsih date
time_list = []
for each in data['job_publish_date']:
    time_list.append(int(each.split()[0].replace('+', '')))

print(time_list)
range = (0, 30)
bins = 10
plt.hist(time_list, bins, histtype='bar')
plt.xlabel('days ago')
plt.ylabel('number of jobs')
plt.show()
