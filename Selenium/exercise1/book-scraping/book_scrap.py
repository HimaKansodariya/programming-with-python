from selenium import webdriver
import time
import pandas as pd
from selenium.common.exceptions import NoSuchElementException

book_details = []
rating_in_words = ['Zero','One','Two','Three','Four','Five']

search_query = 'http://books.toscrape.com/'
driver = webdriver.Chrome("C:\\Users\\Weboccult\\Desktop\\Hima\\chromedriver.exe")
driver.get(search_query)
time.sleep(2)

while True:
    try:
        book_list = driver.find_elements_by_xpath('//article[@class="product_pod"]')
        for each_book in book_list:
            # getting book info
            book_name = each_book.find_elements_by_tag_name("h3")[0]
            rating_word = each_book.find_element_by_class_name('star-rating').get_attribute('class').split(' ')[-1]
            rating = rating_in_words.index(rating_word)
            price = each_book.find_elements_by_class_name('price_color')[0]
            availability = each_book.find_element_by_class_name('availability').get_attribute('class').split(' ')[0]
            # saving book info
            book_info = [book_name.text, rating, price.text, availability]
            book_details.append(book_info)
        # going to next page
        next_link = driver.find_element_by_xpath('//li[@class="next"]/a')
        next_link.click()
    # break while loop if next button not found
    except NoSuchElementException:
        break
driver.quit()
# saving info as .csv file
book_details_df = pd.DataFrame(book_details)
book_details_df.columns = ['book name', 'rating', 'price', 'availability']
book_details_df.to_csv('book_details.csv')
