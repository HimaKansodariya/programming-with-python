### EXERCISE 1 (Selenium):

* Choose a website on which you want to do Data Analysis.
* Crawl through website and collect Data.
* Save Data in format of xml, csv, JSON, etc...  (any one)
* Do Analysis of Data using Numpy and Pandas
* Generate insights of Data.
* Create Graphs (If you want)
* Create proper Readme.
* In Gihub include all the Files in Proper Structure Python crawler file, Notebook file, Datafile ,etc... (write code as per clean code book)
Tools to Use: Anaconda, Numpy, Pandas, selenium, etc...
  
### Understanding & Execution:

#### scraping 
* choose a commercial based website for data scraping http://books.toscrape.com/
* get a data of given url using webdriver
* get information of all product using find_element/find_elements
* get following information 
    * book name
    * book rating
    * book price
    * book availability
* to get information of all webpages of chosen website, use while a loop to click on navigation button until next button is not found
* when navigation button not found, except the exception and break the while loop
* store all information in 2D list
* quite driver
* convert information list into DataFrame using pandas
* give column names and covert DataFrame into .csv file and store the file.

#### analysis

* read the .csv file to analyse the scraped data
###### rating analysis
* use pandas groupby to group the rating column of data file
* count the number of book for given rating
* store rating into a list and number of books into other list
* plot graph for rating VS num of book using matplotlib 

###### price analysis
* get the price column from data
* remove euro sign and convert string into float value for easy access during analysis
* sort the price array to get minimum and maximum price
* make a range of min price and max price and plot a graph for price range
* hist bar graph will show number of books into given price range


~~~~


