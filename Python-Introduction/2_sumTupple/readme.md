#### EXERCISE 2:
Python program to add to poaitive integers in tupple without using the '+' operator.

###### Example:
* Input=>(2,3) Output=> 5
* Input=>(4,6) Output=>10

##### UNDERSTANDING:
I need to create a function which takes tuple(containing two positive integer) as an argument and return sum of those two integer. But to perform summation, '+' (operation) can not be used.
here, to perform sum I have used python in-built function sum(). tuple can be passed as argument in sum() function and it returns addition of all numbers of tuple. 

##### EXECUSION:

* creat a function sumTuple having 1 parameter myTuple 
* inside the function sumTuple(myTuple)
	* return sum of numbers using sum() function. 




