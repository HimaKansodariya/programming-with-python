from sumTuple_run_2 import *
import json
file = open("test_cases.json", "r").read()
output = open("output.txt", "w")
testcases = json.loads(file)

for case in testcases['cases']:
    if case['output'] == sumTuple(tuple(case['input'])):
        print('success')
        output.write('success\n')
    else:
        print('failed')
        output.write('failed\n')
output.close()