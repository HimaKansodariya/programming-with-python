#### EXERCISE:4
Python program to extract the year, month, date, and time using Lambda.

##### Example:
* Input => 2020-01-15 09:03:32.744178
* Output =>Y=2020 => M=1 => D=15 => H=09 => M=03 => S=32.744178

##### UNDERSTANDING:
I need to create a function which takes string as an argument. Using lambda function I need to extract and print saparate values of year, month, day, hour, minutes, seconds.

##### EXECUSION:
* create a function having one parameter(string)
* inside the function  

    *   type casting of string argument to datetime 
    *   get value of year, month, day, hour, minutes, seconds using inbuilt methods inside lambda function
    *   return all value inside a list 
    *   print all the values



