from datetime import datetime
def DateTime(userInput):
    x = datetime.strptime(userInput, '%Y-%m-%d %H:%M:%S.%f')
    year = lambda x: x.year
    month = lambda x: x.month
    day = lambda x: x.day
    hour = lambda x: x.hour
    minute = lambda x: x.minute
    second = lambda x: x.second
    msec = lambda x: x.microsecond
    print('Y =',year(x),'\nM =',month(x),'\nD =',day(x),'\nH =',hour(x),'\nM =',minute(x),'\nS =',str(second(x))+'.'+str(msec(x)))
    return [year(x), month(x), day(x), hour(x), minute(x), second(x), msec(x)]