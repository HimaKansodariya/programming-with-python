from dateTimeLambda_run_4 import *
import json
file = open("test_cases.json", "r").read()
output = open("output.txt", "w")
testcases = json.loads(file)

for case in testcases['cases']:
    if case['output'] == DateTime(case['input']):
        print('success')
        output.write('success\n')
    else:
        print('failed')
        output.write('failed\n')
output.close()