#### EXERCISE:5
Python program to get a list, sorted in increasing order by the last element in each tuple from a given list of non-empty tuples.

###### Example:
* Input => [(2, 5), (1, 2), (4, 4), (2, 3), (2, 1)]
* Output =>[(2, 1), (1, 2), (2, 3), (4, 4), (2, 5)]

##### UNDERSTANDING:
I need to create a function which takes list(containing tuples) as an argument and sort the list in increasing order considering last number of tuple.

##### EXECUSION:
* create a function having one parameter(list)
* inside the function 

	* use inbuilt function sort() to sort the list
	* pass key in sort() as last index of tuple so that everytime we will get last value of tuple and list will be sorted.
	* return sorted list


