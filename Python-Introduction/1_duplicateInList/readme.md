#### EXERCISE 1:
Python function that takes a sequence of numbers as list and determines whether all the numbers are different from each other.

###### Example
* Input=>[1,2,3,4,5,6,7,8,9] Output=> true
* Input=>[0,1,2,3,4,0] Output=>false

##### UNDERSTANDING: 
Here, in given task I need to take list an and input. list will contain sequence of numbers. I need to check whether all numbers in list are different.
Return True if all numbers in list are different
return False if same number exist

##### EXECUTION: 
list can contain duplticate element, set can not contain duplicate element. So convert list into set and check length of both list & set. If all numbers are different then length of list and set will be same & we will get True value. Else we will get False value.


