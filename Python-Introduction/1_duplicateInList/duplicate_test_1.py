from duplicate_run_1 import *
import json
file = open("test_cases.txt", "r").read()
output = open("output.txt", "w")
testcases = json.loads(file)

for case in testcases['cases']:
    if case['output'] == duplicate(case['input']):
        print('success')
        output.write('success\n')
    else:
        print('failed')
        output.write('failed\n')
output.close()