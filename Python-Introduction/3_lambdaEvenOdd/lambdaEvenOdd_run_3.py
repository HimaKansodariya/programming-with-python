def EvenOdd(myList):
    evenList = list(filter(lambda num:num%2==0, myList))
    oddList = list(filter(lambda num:num%2==1, myList))
    return [evenList, oddList]
