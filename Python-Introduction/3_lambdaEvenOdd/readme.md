#### EXERCISE 3:
Python program to filter(even and odd) a list of integers using Lambda.

###### Example:
* Input=>[1, 2, 3, 4, 5, 6, 7, 8, 9, 10] Output=> [2, 4, 6, 8, 10] and [1, 3, 5, 7, 9]
* Input=>[1, 3, 4] Output=> [4] and [1, 3]

##### UNDERSTANDING:
I need to create a function which takes list(containing sequence of integers) as an argument and using lambda function, it filters all input integer into even and odd. Returns two list, 1 containing all even number and other containing all odd numbers. 

##### EXECUSION:
* create a function having one parameter list. 
* inside the function 
    * To get list of even number, use n%2==0 condition inside lambda function & filter all value of inputList using filter()
    * To get list of odd number, use n%2==1 condition inside lambda function & filter all value of inputList using filter()




