##### EXERCISE 1
* Write a Class which has all functionality of a list. 
* create a method append which only appends values which are divisible by 5. Else raise an exception NotDivisibleBy5. While appending divide that value by 5 
* This contains another method which lets me check that all elements in the array are even.

##### Example
* input => (This is single input and output Structure)
custom_list_obj=CustomList() \
</br>custom_list_obj.append(6) //(raise exception) continue even after exception to interpret new line
</br>print(custom_list_obj)
</br>custom_list_obj.append(5) //add 1 in list because 5/5=1
</br>print(custom_list_obj)
</br>custom_list_obj.append(10) //add 2 in list because 10/5=2
</br>print(custom_list_obj)
</br>custom_list_obj.insert(0,8) //insert 8 value at index 0
</br>print(custom_list_obj)
</br>print(custom_list_obj.is_all_even())

* Output =
</br>[]
</br>[1]
</br>[1,2]
</br>[8,1,2]
</br>[False]
  
#### UNDERSTANDING
I need to create a class CustomList which have all functionality all inbuilt class 'list' and have method to customize inbuilt append method. 
that append method only append (after dividing it by 5) those elements which are divisible by 5. else it will raise exception.

there will be another method in same class to check if all elements in list are even or not. If all are even then it will return True, else it will return False

There will a one more class of exception NotDivisibleBy5 for customized exception

#### EXECUTION

* create a class for exception NotDivisibleBy5
* create a class CustomList
    * create a append method
    * check if passed argument is divisible by 5
    * if true
        * divide number by 5 and append result into list
    * else
        * raise exception NotDivisbleBy5
    * except error and print
  
    * create a method to check even number
    * set flag true and if there is an odd element set flag false and break it
    * return value of flag


