class Animal:
    __msg = 'I am animal'
    leg = 'I have 4 legs'
    def get_info(self):
        return self.__msg
    def count_leg(self):
        return self.leg

class Wild(Animal):
    __msg = 'I am wild'
    food = 'I eat wild food'
    def get_info(self):
        x = self.__msg + '\n' + super().count_leg() + '\n' + super().get_info()
        return x
    def get_food(self):
        return self.food

class Pet(Animal):
    __msg = 'I am pet'
    food = 'I eat pet food'
    def get_info(self):
        x = self.__msg + '\n' + super().count_leg() + '\n' + super().get_info()
        return x
    def get_food(self):
        return self.food

class Canine(Animal):
    __msg = 'I am canine'
    food = 'I eat canine food'
    def get_info(self):
        x = self.__msg + '\n' + super().count_leg() + '\n' + super().get_info()
        return x
    def get_food(self):
        return self.food

class Feline(Animal):
    __msg = 'I am Feline'
    food = 'I eat Feline food'
    def get_info(self):
        x = self.__msg + '\n' + super().count_leg() + '\n' + super().get_info()
        return x
    def get_food(self):
        return self.food

class Dog(Pet,Canine):
    __msg = 'I am Dog'
    speak = 'I do bhaw bhaw'

    def get_info(self):
        x = self.__msg + '\n' + super().get_food() + '\n' + super(Dog, self).get_info()
        return x
    def get_speak(self):
        return self.speak

class Fox(Wild,Canine):
    __msg = 'I am fox'
    speak = 'I do uuuuummm'
    def get_info(self):
        x = self.__msg + '\n' + super().get_food() + '\n' + super(Fox, self).get_info()
        return x
    def get_speak(self):
        return self.speak

class Leopard(Wild,Feline):
    __msg = 'I am Leopard'
    speak = 'I do roarrrr'
    def get_info(self):
        x = self.__msg + '\n' + super().get_food() + '\n' + super(Leopard, self).get_info()
        return x
    def get_speak(self):
        return self.speak

class Cat(Pet,Feline):
    __msg = 'I am cat'
    speak = 'I do meowwww'
    def get_info(self):
        x = self.__msg + '\n' + super().get_food() + '\n' + super(Cat, self).get_info()
        return x
    def get_speak(self):
        return self.speak


a = Dog()
print(a.get_info())
print(a.get_speak())

