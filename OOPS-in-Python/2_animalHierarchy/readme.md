#### EXERCISE 2

Use Following class hierarchy to create classes.

* animal (get_info, count_legs)
    * wild (get_info, get_food)
        * leopard (get_info, get_name)
        * fox (get_info, get_name)
    * Pet (get_info, get_food)
        * leopard (get_info, get_name)
        * fox (get_info, get_name)
    * Canine (get_info, get_food)
        * leopard (get_info, get_name)
        * fox (get_info, get_name)
    * Frline (get_info, get_food)
        * leopard (get_info, get_name)
        * fox (get_info, get_name)
    
Create a simple message flow to get flow from all the classes to dog,cat,leopard and fox
    
#### UNDERSTANDING

* HERE I need to create a class 'Animal'. 
* after that create 4 class wild, pet, canine, feline. All of these will inherit Animal class
* create 4 class dog, cat, fox, leopard
    * dog will inherit pet & canine
    * cat will inherit pet & feline
    * leopard will inherit feline
    * fox will inherit wild & canine

#### EXECUTION

* create a class as mentioned above.
* define methods according to class 
*  inherit method of parent class use super and use it in child class
* this will giv all info of parent class one a method of class is called