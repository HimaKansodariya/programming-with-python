#### EXERCISE 1:

Draw following pattern for user input n

#### UNDERSTANDING

to print the pattern we need to take rows and columns in number line because pattern is creating replica horizontally and vertically 

#### EXECUTION

* here total 2 loops will be needed 1 for rows and nested for loop for column
* total 3 condition will be there
    * if to print 0
    * elif to print *
    * else to print space